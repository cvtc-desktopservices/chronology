# pylint: disable=missing-module-docstring,missing-function-docstring,protected-access

import datetime

import pytest

from chronology import notation


@pytest.mark.parametrize(
    "test_input, expected",
    [
        ("0155", datetime.time(1, 55)),
        ("0030", datetime.time(0, 30)),
        ("2359", datetime.time(23, 59)),
        ("2144", datetime.time(21, 44)),
        ("1321", datetime.time(13, 21)),
    ],
)
def test_time_from_24h_notation(test_input, expected):
    assert notation.time_from_24h_notation(test_input) == expected


def test_time_from_24h_notation_fail_3_characters():
    with pytest.raises(ValueError):
        notation.time_from_24h_notation("110")


def test_time_from_24h_notation_fail_letters():
    with pytest.raises(ValueError):
        notation.time_from_24h_notation("asdf")
