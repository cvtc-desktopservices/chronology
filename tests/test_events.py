# pylint: disable=missing-module-docstring,missing-function-docstring,protected-access

import datetime

from chronology import events


def test_weekly_dates_between():
    start_date = datetime.date(2019, 10, 25)
    end_date = datetime.date(2019, 11, 26)

    assert events.NaiveRecurringEvent._weekly_dates_between(start_date, end_date) == [
        datetime.date(2019, 10, 25),
        datetime.date(2019, 11, 1),
        datetime.date(2019, 11, 8),
        datetime.date(2019, 11, 15),
        datetime.date(2019, 11, 22),
    ]


def test_weekly_dates_between_inclusive():
    start_date = datetime.date(2019, 10, 25)
    end_date = datetime.date(2019, 11, 29)

    assert events.NaiveRecurringEvent._weekly_dates_between(start_date, end_date) == [
        datetime.date(2019, 10, 25),
        datetime.date(2019, 11, 1),
        datetime.date(2019, 11, 8),
        datetime.date(2019, 11, 15),
        datetime.date(2019, 11, 22),
        datetime.date(2019, 11, 29),
    ]


def test_naive_weekly_events_between():
    # These dates selected because Daylight Savings Time ended on November
    # 3, 2019
    start_date = datetime.date(2019, 10, 25)
    start_time = datetime.time(12, 0)
    end_date = datetime.date(2019, 11, 8)
    end_time = datetime.time(13, 0)

    expected = [
        events.NaiveOccurence(
            datetime.datetime(2019, 10, 25, 12, 0, 0, 0),
            datetime.datetime(2019, 10, 25, 13, 0, 0, 0),
        ),
        events.NaiveOccurence(
            datetime.datetime(2019, 11, 1, 12, 0, 0, 0),
            datetime.datetime(2019, 11, 1, 13, 0, 0, 0),
        ),
        events.NaiveOccurence(
            datetime.datetime(2019, 11, 8, 12, 0, 0, 0),
            datetime.datetime(2019, 11, 8, 13, 0, 0, 0),
        ),
    ]

    assert (
        events.NaiveRecurringEvent._weekly_naive_occurrences_between(
            start_date, end_date, start_time, end_time
        )
        == expected
    )
