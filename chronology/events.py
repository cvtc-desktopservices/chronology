# pylint: disable=too-few-public-methods
"""Abstractions for some common types of events that Chronology can handle"""

from datetime import date, datetime, time, timedelta
from typing import List

from .notation import ISODayOfWeek

SEVEN_DAYS = timedelta(days=7)


class NaiveOccurence:
    """An event occurrence which consists of a naive datetime for start and end.

    The event can be said to be occuring between start and end. Before start, the
    event is going to happen. After end, the event has happened.
    """

    def __init__(self, start, end):
        self.start = start
        self.end = end

    start: datetime
    """A naive datetime object representing the beginning of the event"""

    end: datetime
    """A naive datetime object representing the beginning of the event"""

    def __eq__(self, other):
        return (self.start == other.start) and (self.end == other.end)


class NaiveRecurringEvent:
    """A simple event which recurs.

    The event occurs on the :class:`ISODayOfWeek` days listed in ``days``
    between start_date and end_date at the time between start_time and
    end_time.

    :param start_date: The first date that the event may possibly occur. The
        first occurrence of the event is only on this date if it is contained
        in ``days``, otherwise the first occurrence occurs the first time
        that a day in ``days`` is reached after ``start_date``

    :param end_date: The last date that the event may possibly occur.

    :param start_time: The time that each occurrence of the event begins.

    :param end_time: The time that each occurrence of the event ends.

    :param days: The days that the event occurs on.
    """

    def __init__(
        self,
        start_date: date,
        end_date: date,
        start_time: time,
        end_time: time,
        days: List[ISODayOfWeek],
    ):  # pylint: disable=too-many-arguments
        self.start_date = start_date
        self.end_date = end_date
        self.start_time = start_time
        self.end_time = end_time
        self.days = days

    @property
    def occurrences(self) -> List[NaiveOccurence]:
        """Every occurrence of this event"""

        event_occurrences = []
        for day in self.days:
            first_occurence = self._first_weekday_after_date(day, self.start_date)

            event_occurrences.extend(
                self._weekly_naive_occurrences_between(
                    first_occurence, self.end_date, self.start_time, self.end_time
                )
            )

        return event_occurrences

    @classmethod
    def _weekly_naive_occurrences_between(
        cls, first_date: date, end_date: date, start_time: time, end_time: time
    ) -> List[NaiveOccurence]:
        """Get an event every seven days between start_time and end_time

        This is needed because the current iCalendar utilities for Python don't
        work with recurring events very well.

        :param first_date: The first day that an event will occur. The first
            event occurs on this date.

        :param end_date: The final day that an event may occur. The final event can
            occur on or before this date, but not after.

        :param start_time: The beginning time of the event.

        :param end_time: The end time of the event.

        :returns: List of :class:`NaiveEvent`, one for each occurence of the event.
        """

        event_dates = cls._weekly_dates_between(first_date, end_date)
        naive_events = []
        for event in event_dates:
            first_datetime = datetime.combine(event, start_time)
            end_datetime = datetime.combine(event, end_time)
            naive_events.append(NaiveOccurence(first_datetime, end_datetime))

        return naive_events

    @classmethod
    def _weekly_dates_between(cls, first_date: date, end_date: date) -> List[date]:
        """Get every day that a weekly event would occur between first_date and end_date

        :param first_date: First day that the weekly event occurs on

        :param end_date: Last day that the weekly event could possibly occur on

        ``end_date`` does not necessarily need to be a multiple of 7 days from
        ``first_date``. Instead, you will receive a list of days beginning from
        ``first_date`` and including ``end_date`` if required.
        """

        event_dates = []
        candidate = first_date
        while candidate <= end_date:
            event_dates.append(candidate)
            candidate = candidate + SEVEN_DAYS

        return event_dates

    @classmethod
    def _first_weekday_after_date(cls, weekday: ISODayOfWeek, start_date: date) -> date:
        """Returns a date object containing the first instance of weekday after date
        For example, if 'date' is Sunday, January 1st, and weekday is Tuesday, then
        Tuesday, January 3rd, will be returned.
        """

        target_weekday = weekday.value

        start_weekday = start_date.isoweekday()
        day_difference = target_weekday - start_weekday
        day_difference_delta = timedelta(days=day_difference)

        return start_date + day_difference_delta
