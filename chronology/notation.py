"""Abstractions that handle notation of different time values"""

from datetime import time
from enum import IntEnum

from typing import List


class ISODayOfWeek(IntEnum):
    """Represents days of weeks.
    The values of this enum can be used as an ISO dow value (1-7) if needed
    """

    monday = 1
    tuesday = 2
    wednesday = 3
    thursday = 4
    friday = 5
    saturday = 6
    sunday = 7


def time_from_24h_notation(timespec: str) -> time:
    """Gets a naive time value from a 24-hour noted time (like 1155 for 11:55)

    This function only accepts four-character strings. Trying to pass "155"
    for 01:55 is not valid. "0155" is valid.

    :param timespec: An integer value of a 24-hour time, such as 1155.

    :raises ValueError: A four-character string was not provided.

    :raises ValueError: The four-character string provided was not numeric.
    """

    if len(timespec) != 4:
        raise ValueError("A four-character timespec must be used.")

    hour = int(timespec[0:2])
    minute = int(timespec[2:4])
    return time(hour, minute)


def days_from_fields(  # pylint: disable=too-many-arguments
    monday, tuesday, wednesday, thursday, friday, saturday, sunday
) -> List[ISODayOfWeek]:
    """Converts seven truthy or falsy fields into ISODayOfWeek values

    Given a field for each of Monday through Sunday, each is checked for its
    truthiness or falsiness. Each truthy field corresponds to a returned
    ISODayOfWeek value in the list.
    """
    days = []
    if bool(monday):
        days.append(ISODayOfWeek.monday)
    if bool(tuesday):
        days.append(ISODayOfWeek.tuesday)
    if bool(wednesday):
        days.append(ISODayOfWeek.wednesday)
    if bool(thursday):
        days.append(ISODayOfWeek.thursday)
    if bool(friday):
        days.append(ISODayOfWeek.friday)
    if bool(saturday):
        days.append(ISODayOfWeek.saturday)
    if bool(sunday):
        days.append(ISODayOfWeek.sunday)
    return days
