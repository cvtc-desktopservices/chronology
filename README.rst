Sidereal Chronology
===================

**Heads up!**

Chronology is currently unmaintained.

Sidereal Chronology provides objects which can be used to better structure schedule data.

Install
-------

Chronology can be installed from this git repository::

    pip install git+https://bitbucket.org/cvtc-desktopservices/chronology/src/master/

Documentation
-------------

Chronology's documentation can be found at https://chronology-cvtc.readthedocs.io/

Requirements
------------

Chronology supports Python 3.6 and above, but is primarily developed using 3.6. If you have a newer version of Python, consider using `pyenv <https://github.com/pyenv/pyenv>`_ to install the older version. We develop on Python 3.6 because it is the newest version of Python supported on Red Hat Enterprise Linux. However, we run tests against Python 3.7 and 3.8 as well.
